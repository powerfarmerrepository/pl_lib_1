package main;

import java.io.IOException;
import java.util.Map;

import org.json.JSONException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;


@Listeners ({ConfigurationListener.class})
public abstract class Main {
   
	public static WebDriver driver;
    public static WebDriverWait wait;
    public static int waitForElement = 20;
    public static Parameters parameters;
	public final static String reportName = "Report.html";
	public static Reporter report = new Reporter(System.getProperty("user.dir")+ System.getProperty("file.separator")+"result"+System.getProperty("file.separator")+"attachments"+System.getProperty("file.separator")+reportName);
    public PfUtils pfu;
    
	 
	@BeforeSuite
    public void tearUp() throws JSONException, IOException   {
    	    
    	  parameters = new Parameters(System.getProperty("user.dir")+ System.getProperty("file.separator"),"UTF-8");
    	  parameters.readParameters();	
          report.startTest(parameters.getServiceName());	
    		
          //przegladarki
          
          
          
          
    	    }
   

    @AfterMethod(alwaysRun=true)
    public void afterMethod(ITestResult result) throws IOException{    	
   
         if (result.getStatus() == ITestResult.FAILURE) { 	
             
        	    String screenShotName = "";  
        	    parameters.setResult("failed", "Service was ended with FAIL status: details are available in Technical information   "+getInputParametersString(), null , null , null); 
        	    report.logFail(result.getThrowable().toString());         
        	    report.logScreenShot(screenShotName);
        	    
         }         	    
    }

       	
    
    @AfterSuite
    public void tearDown(ITestContext context) throws JSONException, IOException{
   	   	   	
        if ( context.getFailedTests().size() == 0 ) {
      	 
		  parameters.setResult("success", "Service was ended with PASS status: details are available in Technical information  "+getInputParametersString(), null, null, null); 
		  report.logPass("Service was ended with PASS status");	
		 
        }  
		
        
    	report.generateRaport();
    
    }
    
    
    
  
  public String getInputParametersString() {
	
	 String resultParams = "'\n'";
     Map<String,String> params = parameters.getMapParams();
     for (String key : params.keySet()) { resultParams = resultParams+key+" : "+params.get(key) + "'\n'" ;}
    		  
	 return resultParams;
	  	  
  }
    
    
    
}

